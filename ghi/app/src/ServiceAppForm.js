import React from 'react';
import { NavLink } from 'react-router-dom'
class CreateApp extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            techs: [],
            vin: "",
            date: '',
            reason: '',
            owner: '',
            is_finished: false,
            hasAppCreated: false,
        };
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }
        delete data.hasAppCreated
        delete data.techs
        const AppsUrl = 'http://localhost:8080/api/apps/'
        const datajson = JSON.stringify(data)
        const fetchConfig = {
            method: 'post',
            body: datajson,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(AppsUrl, fetchConfig)
        if (response.ok) {
            const newApp = await response.json()
            const cleared = {
                tech: '',
                vin: "",
                date: '',
                reason: '',
                owner: '',
                is_finished: false,
                hasAppCreated: true,
            };
            this.setState(cleared)
        } else {
            throw new Error('response is not ok')
        }
    }
    async componentDidMount() {
        const TechUrl = "http://localhost:8080/api/techs/"
        const TResponse = await fetch(TechUrl)
        if (TResponse.ok) {
            const data = await TResponse.json()
            this.setState({ 'techs': data.techs })
        } else {
            throw new Error('response is not ok')
        }
    }

    render() {
        let successAlert = "alert alert-success d-none mb-0"
        let formClass = '';
        if (this.state.hasAppCreated) {
            successAlert = "alert alert-success mb-0"
            formClass = 'd-none';
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create A New Appointment</h1>
                        <form
                            className={formClass}
                            onSubmit={this.handleSubmit}
                            id="create-appointment-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Vin"
                                    required type="text"
                                    id="vin"
                                    name="vin"
                                    className="form-control"
                                    value={this.state.vin} />
                                <label htmlFor="vin">Vin</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Date"
                                    required type="datetime-local"
                                    id="datetime-local"
                                    name="date"
                                    className="form-control"
                                    value={this.state.date} />
                                <label htmlFor="date">Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Reason"
                                    required type="text"
                                    id="reason"
                                    name="reason"
                                    className="form-control"
                                    value={this.state.reason} />
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Owner"
                                    required type="text"
                                    id="owner"
                                    name="owner"
                                    className="form-control"
                                    value={this.state.owner} />
                                <label htmlFor="owner">Owner</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleInputChange}
                                    name="tech"
                                    id="tech"
                                    className="form-select"
                                    value={this.state.tech}
                                    required>
                                    <option value="">Choose a Technician</option>
                                    {this.state.techs.map(tech => {
                                        return (
                                            <option key={tech.id} value={tech.id}>
                                                {tech.name}
                                            </option>
                                        )
                                    })};
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div
                            className={successAlert}
                            id="success-message">
                            Congratulations! You Created an Appointment!
                            <div>
                                <button className="btn-block">
                                    <NavLink className="nav-link" to="/apps/list/">Go To List of Appointments</NavLink>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default CreateApp;
