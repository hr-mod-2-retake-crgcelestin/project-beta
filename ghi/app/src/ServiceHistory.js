import React from 'react';
class ServiceHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: '',
            Apps: [],
        };
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleOnClick = this.handleOnClick.bind(this)
    }
    async handleOnClick(event) {
        const AppsUrl = `http://localhost:8080/api/apps/${event}/`
        const appsResponse = await fetch(AppsUrl)
        if (appsResponse.ok) {
            const data = await appsResponse.json()
            this.setState({ 'Apps': data.App })
        } else {
            alert('invalid search, please try a valid vin')
        }
    }
    async handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    render() {
        return (
            <>
                <p></p>
                <div className="input-group mb-3">
                    <input
                        onChange={this.handleInputChange}
                        type="text"
                        id="vin"
                        name="vin"
                        className="form-control"
                        placeholder="VIN" aria-label="VIN"
                        aria-describedby="basic-addon2"
                        value={this.state.vin}
                    />
                    <div className="input-group-append">
                        <button
                            className="btn btn-outline-secondary"
                            type="button"
                            onClick={() => this.handleOnClick(this.state.vin)}
                        >Search VIN</button>
                    </div>
                </div>
                <h1> Service Appointments</h1>
                <div className="container-fluid p-4 my-1">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">VIN</th>
                                <th scope="col">Customer Name</th>
                                <th scope="col">Date</th>
                                <th scope="col">Time</th>
                                <th scope="col">Technician</th>
                                <th scope="col">Reason</th>
                                <th scope="col">Finished</th>
                                <th scope="col">Canceled</th>
                                <th scope="col">VIP</th>

                            </tr>
                        </thead>
                        <tbody>
                            {this.state.Apps.map((app) => {
                                return (
                                    <tr key={app.id}>
                                        <td>{app.vin}</td>
                                        <td>{app.owner}</td>
                                        <td>
                                            {new Date(app.date).toLocaleDateString(
                                                "en-US"
                                            )
                                            }
                                        </td>
                                        <td>
                                            {new Date(app.date).toLocaleTimeString(
                                                "en-US"
                                            )}
                                        </td>
                                        <td>{app.tech.name}</td>
                                        <td>{app.reason}</td>
                                        <td>{app.is_finished.toString()}</td>
                                        <td>{app.is_canceled.toString()}</td>
                                        <td>{app.vip.toString()}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </>
        )

    }
}

export default ServiceHistory;
