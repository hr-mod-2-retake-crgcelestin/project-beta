import React from 'react';
import { NavLink } from 'react-router-dom'
class CreateAuto extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            color: "",
            year: "",
            vin: "",
            model_id: "",
            models: [],
            hasAutoCreated: false,
        };
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }
        delete data.models
        delete data.hasAutoCreated
        const AutoUrl = 'http://localhost:8100/api/automobiles/'
        const datajson = JSON.stringify(data)
        const fetchConfig = {
            method: 'post',
            body: datajson,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(AutoUrl, fetchConfig)
        if (response.ok) {
            const newAuto = await response.json()
            const cleared = {
                color: "",
                year: "",
                vin: "",
                model_id: "",
                hasAutoCreated: true,
            };
            this.setState(cleared)
        } else {
            throw new Error('response is not ok')
        }
    }
    async componentDidMount() {
        const ModUrl = 'http://localhost:8100/api/models/'
        const ModResponse = await fetch(ModUrl)
        if (ModResponse.ok) {
            const data = await ModResponse.json()
            this.setState({
                'models': data.models
            })
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        let successAlert = "alert alert-success d-none mb-0"
        let formClass = '';
        if (this.state.hasAutoCreated) {
            successAlert = "alert alert-success mb-0"
            formClass = 'd-none';
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create A New Automobile</h1>
                        <form
                            className={formClass}
                            onSubmit={this.handleSubmit}
                            id="create-vehicle-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Color"
                                    required type="text"
                                    id="color"
                                    name="color"
                                    className="form-control"
                                    value={this.state.color} />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="year"
                                    required type="text"
                                    name="year"
                                    id="year"
                                    className="form-control"
                                    value={this.state.year} />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="vin"
                                    required type="text"
                                    name="vin"
                                    id="vin"
                                    className="form-control"
                                    value={this.state.vin} />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    onChange={this.handleInputChange}
                                    name="model_id"
                                    id="model_id"
                                    className="form-select"
                                    value={this.state.model_id}
                                    required>
                                    <option value="">Choose a Model</option>
                                    {this.state.models.map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>
                                                {model.name}
                                            </option>
                                        )
                                    })};
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div
                            className={successAlert}
                            id="success-message">
                            Congratulations! You Created an Automobile!
                            <div>
                                <button className="btn-block">
                                    <NavLink className="nav-link" to="/automobiles/list/">Go To List of Automobiles</NavLink>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default CreateAuto;
