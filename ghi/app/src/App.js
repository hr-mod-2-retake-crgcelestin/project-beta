import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import AutomobileList from './AutomobileList';
import Nav from './Nav';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SalesRecordForm from './SalesRecordForm';
import SalesList from './SalesList';
import SalesRecordDetail from './SalesRecordDetail';
import CreateVehicle from './CreateVehicleModel';
import VehicleList from './VehicleModelList';
import CreateAuto from './CreateAutomobile';
import CreateTech from './TechnicianForm';
import CreateApp from './ServiceAppForm';
import ListApp from './ListApps';
import ListTechs from './TechList';
import ServiceHistory from './ServiceHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="vehicles">
            <Route path="list" element={<VehicleList />} />
            <Route path="new" element={<CreateVehicle />} />
          </Route>
          <Route path="automobiles">
            <Route path='list' element={<AutomobileList />} />
            <Route path="new" element={<CreateAuto />} />
          </Route>
          <Route path='manufacturers'>
            <Route path='list' element={<ManufacturerList />} />
            <Route path='new' element={<ManufacturerForm />} />
          </Route>
          <Route path="techs">
            <Route path="list" element={<ListTechs />} />
            <Route path="new" element={<CreateTech />} />
          </Route>
          <Route path="apps">
            <Route path="new" element={<CreateApp />} />
            <Route path="list" element={<ListApp />} />
            <Route path="search" element={<ServiceHistory />} />
          </Route>
          <Route path='salespeople' element={<SalesPersonForm />} />
          <Route path='customers' element={<CustomerForm />} />
          <Route path='salesrecords' element={<SalesRecordForm />} />
          <Route path='saleslist' element={<SalesList />} />
          <Route path='salesrecorddetail' element={<SalesRecordDetail />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
