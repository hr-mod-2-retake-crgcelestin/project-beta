import React from "react";
import { NavLink } from 'react-router-dom';

class AutomobileList extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            "autos": []
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/automobiles/'
        let response = await fetch(url)

        if (response.ok) {
            let data = await response.json()
            this.setState({ 'autos': data.autos })
        }
    }

    render() {
        return (
            <div className="container-fluid p-4 my-1">
                <h1> Automobiles </h1>
                <table className="table table-bordered ">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Color</th>
                            <th>Year</th>
                            <th>Model</th>
                            <th>Manufacturer</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.state.autos.map((automobile) => {
                            return (
                                <tr className="w-25 p-3" key={automobile.id}>
                                    <td> {automobile.vin} </td>
                                    <td> {automobile.color} </td>
                                    <td> {automobile.year} </td>
                                    <td> {automobile.model.name} </td>
                                    <td> {automobile.model.manufacturer.name} </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }




}

export default AutomobileList
