import React from "react";
import { NavLink } from 'react-router-dom'

class ManufacturerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            hasManufactureCreated: false,
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value
        });
    }

    // handle submit
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.hasManufactureCreated
        const url = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            const cleared = {
                name: '',
                hasManufactureCreated: true,
            }
            this.setState(cleared)
        } else {
            throw new Error('reposnse is not ok')
        }
    }



    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }



    render() {
        let successAlert = "alert alert-success d-none mb-0"
        let formClass = '';
        if (this.state.hasManufactureCreated) {
            successAlert = "alert alert-success mb-0"
            formClass = 'd-none';
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Manufacturer</h1>
                        <form
                            className={formClass}
                            onSubmit={this.handleSubmit}
                            id="create-manufacturer-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeName} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Manufacturer Name</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div
                            className={successAlert}
                            id="success-message">
                            Congratulations! You Created an Automobile!
                            <div>
                                <button className="btn-block">
                                    <NavLink className="nav-link" to="/manufacturers/list/">Go To List of Manufacturers</NavLink>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );

    }
}


export default ManufacturerForm
