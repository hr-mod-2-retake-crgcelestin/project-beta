import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li>
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <div className="dropdown">
							<button
								className="btn dropdown-toggle text-white"
								type="button"
								id="dropdownMenuButton1"
								data-bs-toggle="dropdown"
								aria-expanded="false">
								Sales
							</button>
							<ul
								className="dropdown-menu"
								aria-labelledby="dropdownMenuButton1">
								<li className="nav-item">
									<NavLink className="dropdown-item" to="/salespeople">Add a Sales Employee</NavLink>
								</li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" aria-current="page" to="/customers">Add a Customer</NavLink>
                 </li>
                 <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/salesrecords">Add a Sale</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/saleslist">All Sales</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/salesrecorddetail">Search by Sales Employee</NavLink>
                  </li>
                </ul>
              </div>

            <div className="dropdown">
              <button
                className='btn dropdown-toggle text-white'
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Service
              </button>
              <ul
                className='dropdown-menu'
                aria-labelledby="dropwdownMenuButton1">
                <li className="nav-item">
                  <NavLink className='dropdown-item' to="/apps/list/">Appointments List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className='dropdown-item' to="/apps/new/">Create a New Appointment</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className='dropdown-item' to="/apps/search/">Search for Appointments Specific to VIN</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className='dropdown-item' to="/techs/new">Create a New Tech</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className='dropdown-item' to="/techs/list/">Technicians</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button
                className='btn dropdown-toggle text-white'
                type="button"
                id="dropdownMenuButton2"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Automobiles
              </button>
              <ul
                className='dropdown-menu'
                aria-labelledby="dropwdownMenuButton2">
                <li className="nav-item">
                  <NavLink className='dropdown-item' to="/automobiles/list/">Automobiles List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="/automobiles/new/">Add an Automobile</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button
                className='btn dropdown-toggle text-white'
                type="button"
                id="dropdownMenuButton3"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Manufacturers
              </button>
              <ul
                className='dropdown-menu'
                aria-labelledby="dropwdownMenuButton3">
                <li className="nav-item">
                  <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/list/">Manufacturers List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="/manufacturers/new">Add a Manufacturer</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button
                className='btn dropdown-toggle text-white'
                type="button"
                id="dropdownMenuButton4"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Vehicles
              </button>
              <ul
                className='dropdown-menu'
                aria-labelledby="dropwdownMenuButton4">
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="/vehicles/list/">Vehicles</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="/vehicles/new">Add a Vehicle</NavLink>
                </li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
